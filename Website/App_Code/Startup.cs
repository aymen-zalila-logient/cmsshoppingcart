﻿using Composite.AspNet.MvcFunctions;
using Composite.Core.Application;
using System.Web.Mvc;

namespace DemoMVC
{
    [ApplicationStartup]
    internal static class StartupHandler
    {
        public static void OnBeforeInitialize()
        {
            var functions = MvcFunctionRegistry.NewFunctionCollection();

            functions.RegisterController<CmsShoppingCart.Controllers.AccountController>(
                "CmsShoppingCart.Account");
			functions.RegisterController<CmsShoppingCart.Controllers.CartController>(
				"CmsShoppingCart.Cart");
			functions.RegisterController<CmsShoppingCart.Controllers.PagesController>(
                "CmsShoppingCart.Pages");
			functions.RegisterController<CmsShoppingCart.Controllers.ShopController>(
                "CmsShoppingCart.Shop");
            functions.RegisterController<CmsShoppingCart.Areas.Admin.Controllers.DashboardController>(
                "CmsShoppingCart.Admin.Dashboard");
			functions.RegisterController<CmsShoppingCart.Areas.Admin.Controllers.PagesController>(
                "CmsShoppingCart.Admin.Pages");
			functions.RegisterController<CmsShoppingCart.Areas.Admin.Controllers.ShopController>(
                "CmsShoppingCart.Admin.Shop");

            functions.RouteCollection.MapMvcAttributeRoutes();
			
			functions.RouteCollection.IgnoreRoute("{resource}.axd/{*pathInfo}");

			functions.RouteCollection.MapRoute(
				"Admin_default",
				"admin/Pages/{action}/{id}",
				new { action = "Index", id = UrlParameter.Optional },
				new { controller = "Pages" },
				new[] { "CmsShoppingCart.Areas.Admin.Controllers" }
				);

			functions.RouteCollection.MapRoute(
				"Admin_Dashboard",
				"admin/Dashboard/{action}/{id}",
				new { action = "Index", id = UrlParameter.Optional },
				new { controller = "Dashboard" },
				new[] { "CmsShoppingCart.Areas.Admin.Controllers" }
				);

			functions.RouteCollection.MapRoute(
				"Admin_Shop",
				"admin/Shop/{action}/{id}",
				new { action = "Index", id = UrlParameter.Optional },
				new { controller = "Shop" },
				new[] { "CmsShoppingCart.Areas.Admin.Controllers" }
				);



			functions.RouteCollection.MapRoute("Account", "Account/{action}/{id}", new { controller = "Account", action = "Index", id = UrlParameter.Optional }, new[] { "CmsShoppingCart.Controllers" });

			functions.RouteCollection.MapRoute("Cart", "Cart/{action}/{id}", new { controller = "Cart", action = "Index", id = UrlParameter.Optional }, new[] { "CmsShoppingCart.Controllers" });

			functions.RouteCollection.MapRoute("Shop", "Shop/{action}/{name}", new { controller = "Shop", action = "Index", name = UrlParameter.Optional }, new[] { "CmsShoppingCart.Controllers" });

			functions.RouteCollection.MapRoute("SidebarPartial", "Pages/SidebarPartial", new { controller = "Pages", action = "SidebarPartial" }, new[] { "CmsShoppingCart.Controllers" });
			functions.RouteCollection.MapRoute("PagesMenuPartial", "Pages/PagesMenuPartial", new { controller = "Pages", action = "PagesMenuPartial" }, new[] { "CmsShoppingCart.Controllers" });
			functions.RouteCollection.MapRoute("Pages", "{page}", new { controller = "Pages", action = "Index" }, new[] { "CmsShoppingCart.Controllers" });
			functions.RouteCollection.MapRoute("Default", "", new { controller = "Pages", action = "Index" }, new[] { "CmsShoppingCart.Controllers" });


			//functions.RouteCollection.MapRoute(
			//	name: "Default",
			//	url: "{controller}/{action}/{id}",
			//	defaults: new { controller = "Pages", action = "Index", id = UrlParameter.Optional }
			//	);

		}

        public static void OnInitialized()
        { }
    }
}