﻿using CmsShoppingCart.Models.Data;
using CmsShoppingCart.Models.ViewModels.Shop;
using PagedList;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace CmsShoppingCart.Areas.Admin.Controllers
{
    public class ShopController : Controller
    {
		// GET: Admin/Shop/Categories
		public ActionResult Categories()
        {
			// Declare list of models
			List<CategoryVM> categories;

			using (Db db = new Db())
			{
				// Init the list
				categories = db.Categories.ToArray().OrderBy(x => x.Sorting).Select(x => new CategoryVM(x)).ToList();
			}

			//Return view with the list
            return View(categories);
        }

		// POST: Admin/Shop/AddNewCategory/categoryName
		[HttpPost]
		public string AddNewCategory(string categoryName)
		{
			// Declare id
			string id = null;

			using (Db db = new Db())
			{
				// Check that the category name is unique
				if (db.Categories.Any(x => x.Name == categoryName))
					return "titletaken";

				// Init DTO
				CategoryDTO dto = new CategoryDTO
				{
					// Add to DTO
					Name = categoryName,
					Slug = categoryName.Replace(" ", "-").ToLower(),
					Sorting = db.Categories.Count()
				};

				// Save DTO
				db.Categories.Add(dto);
				db.SaveChanges();

				//Get the id
				//id = db.Categories.LastOrDefault().Id.ToString();
				id = dto.Id.ToString();
			}

			// Return id
			return id;
		}

		// GET: Admin/Shop/DeleteCategory/id
		[HttpGet]
		public ActionResult DeleteCategory(int id)
		{
			using (Db db = new Db())
			{
				//Get the category
				CategoryDTO dto = db.Categories.Find(id);

				// Remove the category
				db.Categories.Remove(dto);

				// Save DTO              
				db.SaveChanges();
			}

			// Redirect
			return RedirectToAction("Categories");
		}

		//POST: /Admin/Shop/ReorderCategories
		[HttpPost]
		public void ReorderCategories(int[] id)
		{
			using (Db db = new Db())
			{
				// Set initial count
				int count = 1;

				// Declare CategoryDTO
				CategoryDTO dto;

				// Set sorting for each page
				foreach (var categoryId in id)
				{
					dto = db.Categories.Find(categoryId);
					dto.Sorting = count;

					db.SaveChanges();

					count++;
				}
			}
		}

		//POST: /Admin/Shop/RenameCategory
		[HttpPost]
		public string RenameCategory(int categoryId, string newCategoryName)
		{
			using (Db db = new Db())
			{
				// Check category name is unique
				if (db.Categories.Any(c => c.Name == newCategoryName))
					return "titletaken";

				// Get DTO
				CategoryDTO dto = db.Categories.Find(categoryId);

				// Edit DTO
				dto.Name = newCategoryName;

				// Save
				db.SaveChanges();
			}
			//Return
			return "The category name has changed!";
		}

		// GET: Admin/Shop/AddProduct
		[HttpGet]
		public ActionResult AddProduct()
		{
			//Init model
			ProductVM model = new ProductVM();

			// Add select list of categories to model
			using (Db db = new Db())
			{
				model.Categories = new SelectList(db.Categories.ToList(), "Id", "Name");
			}

			//Return view with model
			return View(model);
		}

		// POST: Admin/Shop/AddProduct
		[HttpPost]
		public ActionResult AddProduct(ProductVM model, HttpPostedFileBase file)
		{
			// Check model state
			if (!ModelState.IsValid)
				return AddProduct();

			// Make sure product name is unique
			using (Db db = new Db())
			{
				if (db.Products.Any(p => p.Name == model.Name))
				{
					ModelState.AddModelError("", "That product name is taken!");
					return AddProduct();
				}
			}

			// Declare product id 
			int id;

			// Init and save productDTO
			using (Db db = new Db())
			{
				ProductDTO dto = new ProductDTO
				{
					Name = model.Name,
					Slug = model.Name.Replace(" ", "-").ToLower(),
					Description = model.Description,
					Price = model.Price,
					CategoryId = model.CategoryId
				};

				CategoryDTO categoryDTO = db.Categories.FirstOrDefault(c => c.Id == model.CategoryId);
				dto.CategoryName = categoryDTO.Name;

				//Save
				db.Products.Add(dto);
				db.SaveChanges();

				// Get inserted id
				id = dto.Id;
			}

			// Set TempData message
			TempData["SM"] = "You have added a product!";

			#region Upload Image

			// Create necessary directories
			var originalDirectory = new DirectoryInfo(string.Format("{0}Images\\Uploads", Server.MapPath(@"\")));

			var pathString1 = Path.Combine(originalDirectory.ToString(), "Products\\");
			var pathString2 = Path.Combine(originalDirectory.ToString(), "Products\\" + id.ToString());
			var pathString3 = Path.Combine(originalDirectory.ToString(), "Products\\" + id.ToString() + "\\Thumbs");
			var pathString4 = Path.Combine(originalDirectory.ToString(), "Products\\" + id.ToString() + "\\Gallery");
			var pathString5 = Path.Combine(originalDirectory.ToString(), "Products\\" + id.ToString() + "\\Gallery\\Thumbs");

			if (!Directory.Exists(pathString1))
				Directory.CreateDirectory(pathString1);

			if (!Directory.Exists(pathString2))
				Directory.CreateDirectory(pathString2);

			if (!Directory.Exists(pathString3))
				Directory.CreateDirectory(pathString3);

			if (!Directory.Exists(pathString4))
				Directory.CreateDirectory(pathString4);

			if (!Directory.Exists(pathString5))
				Directory.CreateDirectory(pathString5);

			// Check if a file was uploaded
			if (file != null && file.ContentLength > 0)
			{
				// Get file extension
				string ext = file.ContentType.ToLower();

				// Verity extension
				if (ext != "image/jpg" &&
					ext != "image/jpeg" &&
					ext != "image/pjpeg" &&
					ext != "image/gif" &&
					ext != "image/x-png" &&
					ext != "image/png")
				{
					using (Db db = new Db())
					{
						model.Categories = new SelectList(db.Categories.ToList(), "Id", "Name");
						ModelState.AddModelError("", "The image was not uploaded - wrong image extension.");
						return View(model);
					}
				}

				// Init image name
				string imageName = file.FileName;

				// Save image name to DTO
				using (Db db = new Db())
				{
					ProductDTO dto = db.Products.Find(id);
					dto.ImageName = imageName;

					db.SaveChanges();
				}

				// Set original and thumb image paths
				var path = string.Format("{0}\\{1}", pathString2, imageName);
				var path2 = string.Format("{0}\\{1}", pathString3, imageName);

				// Save original
				file.SaveAs(path);

				// Create and save thumb
				WebImage img = new WebImage(file.InputStream);
				img.Resize(200, 200);
				img.Save(path2);

			}

			#endregion

			// Redirect
			return RedirectToAction("AddProduct");
		}

		// GET: /Admin/Shop/Products/page
		[HttpGet]
		public ActionResult Products(int? page, int? categoryId)
		{
			// Declare list of models
			List<ProductVM> products;

			// Set page number
			var pageNumber = page ?? 1;

			using (Db db = new Db())
			{
				// Init the list
				products = db.Products.ToArray()
					.Where(p => p.CategoryId == categoryId || categoryId == null || categoryId == 0)
					.Select(p => new ProductVM(p)).ToList();

				// Populate categories select list
				ViewBag.Categories = new SelectList(db.Categories.ToList(), "Id", "Name");

				// Set selected category
				ViewBag.SelectedCategory = categoryId.ToString();
			}

			// Set pagination
			var onePageOfProducts = products.ToPagedList(pageNumber, 4);
			ViewBag.OnePageOfProducts = onePageOfProducts;

			//Return view with the list
			return View(products);
		}

		// GET: Admin/Shop/EditProduct/id
		[HttpGet]
		public ActionResult EditProduct(int id)
		{
			// Declare productVM
			ProductVM model;

			using (Db db = new Db())
			{
				// Get the product
				ProductDTO dto = db.Products.Find(id);

				// Make sure product exists
				if (dto == null)
					return Content("This product does not exist.");

				// Init model
				model = new ProductVM(dto)
				{
					// Make a select list
					Categories = new SelectList(db.Categories.ToList(), "Id", "Name"),

					// Get all gallery images
					GalleryImages = Directory.EnumerateFiles(Server.MapPath("~/Images/Uploads/Products/" + id + "/Gallery/Thumbs"))
						.Select(fn => Path.GetFileName(fn))
				};
			}

			// Return view with model
			return View(model);
		}

		// POST: Admin/Shop/EditProduct
		[HttpPost]
		public ActionResult EditProduct(ProductVM model, HttpPostedFileBase file)
		{
			// Get product id
			int id = model.Id;

			// Populate categories select list and gallery images
			using (Db db = new Db())
			{
				model.Categories = new SelectList(db.Categories.ToList(), "Id", "Name");
			}
			model.GalleryImages = Directory.EnumerateFiles(Server.MapPath("~/Images/Uploads/Products/" + id + "/Gallery/Thumbs"))
					.Select(fn => Path.GetFileName(fn));

			// Check model state
			if (!ModelState.IsValid)
				return View(model);

			using (Db db = new Db())
			{
				//Get the product
				ProductDTO dto = db.Products.Find(model.Id);

				// Confirm product exists
				if (dto == null)
					return Content("The product does not exists.");

				// Make sure product name is unique
				if (db.Products.Where(p => p.Id != id).Any(p => p.Name == model.Name))
				{
					ModelState.AddModelError("", "That product name is taken!");
					return View(model);
				}

				// DTO the rest
				dto.Name = model.Name;
				dto.Slug = model.Name.Replace(" ", "-");
				dto.Description = model.Description;
				dto.Price = model.Price;
				dto.ImageName = model.ImageName;
				dto.CategoryId = model.CategoryId;

				CategoryDTO catDto = db.Categories.FirstOrDefault(c => c.Id == model.CategoryId);
				dto.CategoryName = catDto.Name;

				// Save DTO              
				db.SaveChanges();
			}

			// Set TempData message
			TempData["SM"] = "You have edited the product!";

			#region Image Upload

			if (file != null && file.ContentLength > 0)
			{
				// Get file extension
				string ext = file.ContentType.ToLower();

				// Verity extension
				if (ext != "image/jpg" &&
					ext != "image/jpeg" &&
					ext != "image/pjpeg" &&
					ext != "image/gif" &&
					ext != "image/x-png" &&
					ext != "image/png")
				{
					using (Db db = new Db())
					{
						//	model.Categories = new SelectList(db.Categories.ToList(), "Id", "Name");
						ModelState.AddModelError("", "The image was not uploaded - wrong image extension.");
						return View(model);
					}
				}

				// Set upload directory paths
				var originalDirectory = new DirectoryInfo(string.Format("{0}Images\\Uploads", Server.MapPath(@"\")));
				
				var pathString1 = Path.Combine(originalDirectory.ToString(), "Products\\" + id.ToString());
				var pathString2 = Path.Combine(originalDirectory.ToString(), "Products\\" + id.ToString() + "\\Thumbs");

				// Delete files from directories
				DirectoryInfo di1 = new DirectoryInfo(pathString1);
				DirectoryInfo di2 = new DirectoryInfo(pathString2);

				foreach (var file2 in di1.GetFiles())
				{
					file2.Delete();
				}
				foreach (var file2 in di2.GetFiles())
				{
					file2.Delete();
				}

				// Save image name
				string imageName = file.FileName;

				using (Db db = new Db())
				{
					ProductDTO dto = dto = db.Products.Find(id);
					dto.ImageName = imageName;

					db.SaveChanges();
				}

				// Save original and thumb images
				var path = string.Format("{0}\\{1}", pathString1, imageName);
				var path2 = string.Format("{0}\\{1}", pathString2, imageName);

				file.SaveAs(path);

				WebImage img = new WebImage(file.InputStream);
				img.Resize(200, 200);
				img.Save(path2);
			}

			#endregion

			// Redirect
			return RedirectToAction("EditProduct");
		}

		// GET: Admin/Shop/DeleteProduct/id
		[HttpGet]
		public ActionResult DeleteProduct(int id)
		{
			using (Db db = new Db())
			{
				//Get the product
				ProductDTO dto = db.Products.Find(id);

				// Remove the product from DB
				db.Products.Remove(dto);
	
				// Save DTO              
				db.SaveChanges();
			}

			// Delete the product from folder	
			var originalDirectory = new DirectoryInfo(string.Format("{0}Images\\Uploads", Server.MapPath(@"\")));

			string pathString = Path.Combine(originalDirectory.ToString(), "Products\\" + id.ToString());

			if (Directory.Exists(pathString))
				Directory.Delete(pathString, true);

			// Redirect
			return RedirectToAction("Products");
		}

		// POST: Admin/Shop/SaveGalleryImages/id
		[HttpPost]
		public void SaveGalleryImages(int? id)
		{
			// Loop through files
			foreach (string filename in Request.Files)
			{
				// TODO voir pourquoi, le parameter n'est pas tranmit correctement
				if (id == null) id = Int32.Parse(Request.UrlReferrer.Segments[Request.UrlReferrer.Segments.Length - 1]);
				// Init the file
				HttpPostedFileBase file = Request.Files[filename];

				// Check it's not null
				if (file != null && file.ContentLength > 0)
				{
					// Set directory paths
					var originalDirectory = new DirectoryInfo(string.Format("{0}Images\\Uploads", Server.MapPath(@"\")));

					string pathString1 = Path.Combine(originalDirectory.ToString(), "Products\\" + id.ToString() + "\\Gallery");
					string pathString2 = Path.Combine(originalDirectory.ToString(), "Products\\" + id.ToString() + "\\Gallery\\Thumbs");
					
					// Save original and thumb images
					var path = string.Format("{0}\\{1}", pathString1, file.FileName);
					var path2 = string.Format("{0}\\{1}", pathString2, file.FileName);

					file.SaveAs(path);

					WebImage img = new WebImage(file.InputStream);
					img.Resize(200, 200);
					img.Save(path2);

				}
			}
		}

		// POST:Admin/Shop/DeleteImage
		[HttpPost]
		public void DeleteImage(int id, string imageName)
		{
			string fullPath1 = Request.MapPath("~/Images/Uploads/Products/" + id + "/Gallery/" + imageName);
			string fullPath2 = Request.MapPath("~/Images/Uploads/Products/" + id + "/Gallery/Thumbs/" + imageName);

			if (System.IO.File.Exists(fullPath1))
				System.IO.File.Delete(fullPath1);
			if (System.IO.File.Exists(fullPath2))
				System.IO.File.Delete(fullPath2);
		}
	}
}