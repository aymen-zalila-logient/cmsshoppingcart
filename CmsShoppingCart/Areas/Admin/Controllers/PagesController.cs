﻿using CmsShoppingCart.Models.Data;
using CmsShoppingCart.Models.ViewModels.Pages;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace CmsShoppingCart.Areas.Admin.Controllers
{
    public class PagesController : Controller
    {
        // GET: Admin/Pages
        public ActionResult Index()
        {
            //Declare list of PageVM
            List<PageVM> pageList;

			using (Db db = new Db())
			{
				//Init the list
				pageList = db.Pages.ToArray().OrderBy(x => x.Sorting).Select(x => new PageVM(x)).ToList();
			}

            //Return view with list
            return View(pageList);
        }
        
        // GET: Admin/Pages/AddPage
        public ActionResult AddPage()
        {
            return View();
        }

        // POST: Admin/Pages/AddPage
        [HttpPost]
        public ActionResult AddPage(PageVM model)
        {
            // Check model state
            if (!ModelState.IsValid)
                return View(model);

            using (Db db = new Db())
            {
                // Declare slug
                string slug;

                // Init pageDTO
                PageDTO dto = new PageDTO();

                // DTO title
                dto.Title = model.Title;

                // Check for and set slug if need be
                if (string.IsNullOrWhiteSpace(model.Slug))
                    slug = model.Title.Replace(" ", "-").ToLower();
                else
                    slug = model.Slug.Replace(" ", "-").ToLower();

                // Make sure title and slug are unique
                if (db.Pages.Any(x => x.Title == model.Title) || db.Pages.Any(x => x.Slug == slug))
                { 
                    ModelState.AddModelError("", "That title or slug already exisets");
                    return View(model);
                }

                // DTO the rest
                dto.Slug = slug;
                dto.Body = model.Body;
                dto.HasSidebar = model.HasSidebar;
                dto.Sorting = db.Pages.Count();

                // Save DTO
                db.Pages.Add(dto);
                db.SaveChanges();
            }

            // Set TempData message
            TempData["SM"] = "You have add a new page!";

            // Redirect
            return RedirectToAction("AddPage");
        }

        // GET: Admin/Pages/EditPage/id
        [HttpGet]
        public ActionResult EditPage(int id)
        {
            // Declare PageVM
            PageVM model;

            using (Db db = new Db())
            {
                //// Init model
                //model = new PageVM(db.Pages.FirstOrDefault(x => x.Id == id));

                //Get the page
                PageDTO dto = db.Pages.Find(id);

                // Confirm page exists
                if (dto == null)
                    return Content("The page does not exists.");

                //Init pageVm
                model = new PageVM(dto);
            }

            // Return view with page
            return View(model);
        }

        // POST: Admin/Pages/EditPage
        [HttpPost]
        public ActionResult EditPage(PageVM model)
        {
            // Check model state
            if (!ModelState.IsValid)
                return View(model);

            using (Db db = new Db())
            {
                // Declare slug
                string slug = "home";

                //Get the page
                PageDTO dto = db.Pages.Find(model.Id);
                
                // Confirm page exists
                if (dto == null)
                    return Content("The page does not exists.");

                // DTO title
                dto.Title = model.Title;

                // Check for and set slug if need be
                if(model.Slug != "home")
                    if (string.IsNullOrWhiteSpace(model.Slug))
                        slug = model.Title.Replace(" ", "-").ToLower();
                    else
                        slug = model.Slug.Replace(" ", "-").ToLower();

                // Make sure title and slug are unique
                //if (db.Pages.Any(x => x.Title == model.Title && x.Id != model.Id) || db.Pages.Any(x => x.Slug == slug && x.Id != model.Id))
                if (db.Pages.Where(x => x.Id != model.Id).Any(x => x.Title == model.Title) || 
                    db.Pages.Where(x => x.Id != model.Id).Any(x => x.Slug == slug))
                    {
                    ModelState.AddModelError("", "That title or slug already exisets");
                    return View(model);
                }

                // DTO the rest
                dto.Slug = slug;
                dto.Body = model.Body;
                dto.HasSidebar = model.HasSidebar;

                // Save DTO              
                db.SaveChanges();
            }

            // Set TempData message
            TempData["SM"] = "You have edited the page!";

            // Redirect
            return RedirectToAction("EditPage");
        }

        // GET: Admin/Pages/PageDetails/id
        [HttpGet]
        public ActionResult PageDetails(int id)
        {
            // Declare PageVM
            PageVM model;

            using (Db db = new Db())
            {
                //Get the page
                PageDTO dto = db.Pages.Find(id);

                // Confirm page exists
                if (dto == null)
                    return Content("The page does not exists.");

                //Init pageVM
                model = new PageVM(dto);
            }

            // Return view with page
            return View(model);
        }

        //GET: Admin/Pages/DeletePage/id
        [HttpGet]
        public ActionResult DeletePage(int id)
        {
            using (Db db = new Db())
            {
                //Get the page
                PageDTO dto = db.Pages.Find(id);

                // Remove the page
                db.Pages.Remove(dto);

                // Save DTO              
                db.SaveChanges();
            }

            // Redirect
            return RedirectToAction("Index");
        }

        //POST: Admin/Pages/ReorderPages
        [HttpPost]
        public void ReorderPages(int[] id)
        {
            using (Db db = new Db())
            {
                // Set initial count
                int count = 1;

                // Declare PageDTO
                PageDTO dto;

                // Set sorting for each page
                foreach (var pageId in id)
                {
                    dto = db.Pages.Find(pageId);
                    dto.Sorting = count;

                    db.SaveChanges();

                    count++;
                }
            }
        }

        // GET: Admin/Pages/EitSidebar
        [HttpGet]
        public ActionResult EditSidebar()
        {
            // Declare PageVM
            SidebarVM model;

            using (Db db = new Db())
            {
                //Get the page
                SidebarDTO dto = db.Sidebar.Find(1);

                // Confirm page exists
                if (dto == null)
                    return Content("The sidebar does not exists.");

                //Init SidebarVM
                model = new SidebarVM(dto);
            }

            // Return view with page
            return View(model);
        }

		// GET: Admin/Pages/EitSidebar
		[HttpPost]
		public ActionResult EditSidebar(SidebarVM model)
		{
			using (Db db= new Db())
			{
				// Get the DTO
				SidebarDTO dto = db.Sidebar.Find(1);

				// DTO the body
				dto.Body = model.Body;

				// Save
				db.SaveChanges();
			}

			// Set TempData message
			TempData["SM"] = "You haveedit the sidebar!";

			//Redirect
			return RedirectToAction("EditSidebar");
		}

	}
}