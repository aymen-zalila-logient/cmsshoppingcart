﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace CmsShoppingCart
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

			routes.MapRoute(
				"Pages",
				"{pages}",
				new { controller = "Pages", action = "Index" },
				new[] { "CmsShoppingCart.Controllers" }
				);

			routes.MapRoute(
				"Default",
				"",
				new { controller = "Pages", action = "Index" },
				new[] { "CmsShoppingCart.Controllers" }
				);

			//routes.MapRoute(
			//	name: "Default",
			//	url: "{controller}/{action}/{id}",
			//	//defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }			
			//	defaults: new { controller = "Default", action = "Index", id = UrlParameter.Optional },
			//	namespaces: new[] { "CmsShoppingCart.Controllers" }
			//);
		}
    }
}
