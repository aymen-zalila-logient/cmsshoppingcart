﻿using CmsShoppingCart.Models.Data;
using CmsShoppingCart.Models.ViewModels.Pages;
using System;
using System.Linq;
using System.Web.Mvc;

namespace CmsShoppingCart.Controllers
{
    public class PagesController : Controller
    {
        // GET: Index/{page}
        public ActionResult Index(string page = "")
        {
			// Get/set page slug
			if (page == "")
				page = "home";

			// Declare model and DTO
			PageVM model;
			PageDTO dto;

			using (Db db = new Db())
			{
				// Check if page exists
				if (!db.Pages.Any(p => p.Slug.Equals(page)))
					return RedirectToAction("Index", new { page = "" });

				// Get the DTO
				dto = db.Pages.Where(p => p.Slug.Equals(page)).FirstOrDefault();

			}
			// Set page title
			ViewBag.PageTitle = dto.Title;

			// Check for sidebar
			if (dto.HasSidebar)
				ViewBag.SideBar = true;
			else
				ViewBag.SideBar = false;

			// Init model
			model = new PageVM(dto);

			//Return view with model
			return View(model);
        }
    }
}